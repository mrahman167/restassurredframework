package com.base.tests;


import com.base.ExtentTestManager;
import com.base.TestBase;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import databasefiles.ConnectToSqlDB;
import databasefiles.DataProvidersData;
import databasefiles.HwReturningEachColumnInAMap;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.sql.SQLException;
import java.util.Map;
import java.util.logging.Logger;

import static databasefiles.HwReturningEachColumnInAMap.getMapFromDBOfSingleRecord;

public class DataBaseTestsEmployee extends TestBase {
    private static final Logger logger = Logger.getLogger(String.valueOf(HwReturningEachColumnInAMap.class));


    @Test(enabled = false)
    public void validateTestRun() {
        //Map<String, String> data = getMapFromDBOfSingleRecord("SELECT * FROM worldOfAutomation.address");
        //System.out.println(data);

        Map<String, String> data2 = getMapFromDBOfSingleRecord("SELECT * FROM worldOfAutomation.employee");
        System.out.println(data2);
    }


    @Test(enabled = false)
    public void validateUserCanGetAllEmployees() {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/all";
        Response response = RestAssured
                .given()
                .when()
                .get(endpoint)
                .then()
                .log()
                .all()
                .extract().response();
        ExtentTestManager.log(response.asString());
    }


    @Test(enabled = false)
    public void validateUserCanGetSpecificEmployees() {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/1";
        Response response = RestAssured
                .given()
                .when()
                .get(endpoint)
                .then()
                .log()
                .all()
                .extract().response();
        ExtentTestManager.log(response.asString());
    }


    @Test(enabled = false)
    public void validateUserCanDeleteAnEmployee() {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/delete/16";
        Response response = RestAssured
                .given()
                .when()
                .delete(endpoint)
                .then()
                .log()
                .all()
                .extract().response();
        ExtentTestManager.log(response.asString());
        logger.info("Connection Successful");
        ConnectToSqlDB.establishDBConnection();
        logger.info("map of query");
        Map<String, String> mapOfData = getMapFromDBOfSingleRecord("SELECT * FROM worldOfAutomation.employee where id=16;");
        System.out.println(mapOfData);
        Assert.assertTrue(mapOfData.isEmpty());
   /*     if (mapOfData.isEmpty()) {
            System.out.println("is deleted");
        } else {
            System.out.println(" is not deleted and its failing the test case");
            Assert.fail();*/

        }



    @Test(enabled = false)
    public void validateUserCanCreateAnEmployee() throws JsonProcessingException {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/add";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "jawaddd");
        jsonObject.put("permanent", 1);
        jsonObject.put("phone_number", 345363);
        jsonObject.put("role", "QA");
        jsonObject.put("city", "Ozone Park");

        JSONArray jsonArray = new JSONArray();
        jsonArray.add(jsonObject);

        System.out.println(jsonArray.toJSONString());

        Response response = RestAssured
                .given()
                .header("Content-Type", "application/json")
                .when()
                .body(jsonArray.toJSONString())
                .post(endpoint)
                .then()
                .log()
                .all()
                .extract().response();

        String jsonOfResponse =response.asString();
        ExtentTestManager.log(jsonOfResponse);
        logger.info("map to json");
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, String> mapOfResponse = objectMapper.readValue(jsonOfResponse, Map.class);


        logger.info("Connection Successful");
        ConnectToSqlDB.establishDBConnection();

        logger.info("map of data from db");
        Map<String, String> mapOfDataFromDB = getMapFromDBOfSingleRecord("SELECT * FROM worldOfAutomation.employee where name='jawaddd';");
        System.out.println(mapOfDataFromDB);

        Assert.assertEquals(mapOfDataFromDB.get("name"),mapOfResponse.get("name"));
        //error because of id.
        //Assert.assertEquals(json.toString(),jsonObject.toString());

        // query db
// from the map get all records --> expected
// parse the response json into a map
// and match  them all with the records


    }


    @Test(enabled = false)
    public void validateUserCanUpdateAnEmployee() throws SQLException, JsonProcessingException {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/update/18";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "mizu");
        jsonObject.put("permanent", 1);
        jsonObject.put("phone_number", 345363);
        jsonObject.put("role", "QA");
        jsonObject.put("city", "Brooklyn");

        Response response = RestAssured
                .given()
                .header("Content-Type", "application/json")
                .when()
                .body(jsonObject.toJSONString())
                .put(endpoint)
                .then()
                .log()
                .all()
                .extract().response();

        ExtentTestManager.log(response.asString());
        logger.info("Connection Successful");
        ConnectToSqlDB.establishDBConnection();
        logger.info("map of query");
        Map<String, String> query = getMapFromDBOfSingleRecord("SELECT * FROM worldOfAutomation.employee where id =18;");
        System.out.println(query);
        logger.info("map to json");
        String json = new ObjectMapper().writeValueAsString(getMapFromDBOfSingleRecord("SELECT * FROM worldOfAutomation.employee where id =18;"));
        System.out.println(json);
        logger.info("json to map");
        ObjectMapper objectMapper = new ObjectMapper();
        String json2 = "{\r\n" + "   \"name\": \"miz\",\r\n" + " \"role\": \"QA\",\r\n" + "  \"city\": \"Brooklyn\",\r\n" + " \"permanent\": \"1\",\r\n" + " \"phone_number\":\"345363\",\r\n" + "\"id\":\"18\"\r\n" + "}";
        try {
            Map<String, String> map = objectMapper.readValue(json2, Map.class);
            System.out.println("Map is " + map);
            System.out.println("Map Size is " + map.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Assert.assertEquals(jsonObject,json2);
        // Assert.assertEquals(query,jsonObject);
        //Assert.assertNotEquals(query, jsonObject);


        // Assert.assertFalse(Boolean.parseBoolean(query.toString()),jsonObject.toJSONString());

    }






   /* @Test
    public void letsDoHw(){
        Map<String, String> data = HwReturningEachColumnInAMap.getMapFromDBOfSingleRecord("SELECT * FROM worldOfAutomation.employee");
        System.out.println(data);
*/
        /*String json = "{ \"id\": 12,"
                + "\"name\": \"md\","
                + "\"permanent\": \"true\","
                + "\"phone_number\": \"718\","
                + "\"role\": \"qa\","
                + "\"city\": \"null\"]}";
        HashMap<String, String> jsonStore = new HashMap<String, String>();
        jsonStore.put("myJson", json);

        System.out.println(jsonStore.get("myJson"));*/


// query db
// from the map get all records --> expected
// parse the response json into a map
// and match  them all with the records


// try to create a employee with different phone numbers
// 9,10,11, empty, null, alphanumeric, alpha, special char, alpha & special char, with space

// storing response json into object/pojo using jackson-databind

   // @Test(enabled = false,dataProvider = "dataProviderForSearch",dataProviderClass = DataProvidersForTest.class)
    @Test(enabled = false)
    public void validateUserCanCreateAnEmployeewithDifferentPhoneNumber() throws JsonProcessingException {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/add";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "jawa");
        jsonObject.put("permanent", 1);
        jsonObject.put("phone_number", 9);
        jsonObject.put("role", "QA");
        jsonObject.put("city", "Ozone Park");


        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("name", "zan");
        jsonObject1.put("permanent", 1);
        jsonObject1.put("phone_number", 10);
        jsonObject1.put("role", "SDET");
        jsonObject1.put("city", "queens");

        JSONObject jsonObject2 = new JSONObject();
        jsonObject2.put("name", "sam");
        jsonObject2.put("permanent", 1);
        jsonObject2.put("phone_number", 11);
        jsonObject2.put("role", "qa");
        jsonObject2.put("city", "queens");

        JSONObject jsonObject3 = new JSONObject();
        jsonObject3.put("name", "sam2");
        jsonObject3.put("permanent", 1);
        jsonObject3.put("phone_number", null);
        jsonObject3.put("role", "qa");
        jsonObject3.put("city", "queens");

        JSONObject jsonObject4 = new JSONObject();
        jsonObject4.put("name", "sam3");
        jsonObject4.put("permanent", 1);
        jsonObject4.put("phone_number", 10);
        jsonObject4.put("role", "qa");
        jsonObject4.put("city", "queens");

        JSONArray jsonArray = new JSONArray();
        jsonArray.add(jsonObject);
        jsonArray.add(jsonObject1);
        jsonArray.add(jsonObject2);
        jsonArray.add(jsonObject3);
        jsonArray.add(jsonObject4);


        System.out.println(jsonArray.toJSONString());

        Response response = RestAssured
                .given()
                .header("Content-Type", "application/json")
                .when()
                .body(jsonArray.toJSONString())
                .post(endpoint)
                .then()
                .log()
                .all()
                .extract().response();

        ExtentTestManager.log(response.asString());

        ConnectToSqlDB.establishDBConnection();
        Map<String, String> datas = getMapFromDBOfSingleRecord("SELECT * FROM worldOfAutomation.employee where name in ('zan','sam','sam2','sam3');");
        System.out.println(datas);
        //Assert.assertEquals(jsonArray, datas);
        //assertion error, query works in mysql but not here.
    }


    @Test(enabled = false,dataProvider = "dataToInsert",dataProviderClass = DataProviders.class)
    public void validateUserCanCreateAnEmployeewithDifferentPhoneNumberUsingDataProviders(Object data) throws JsonProcessingException {

        DataProvidersData.dataa(data);

    }
}