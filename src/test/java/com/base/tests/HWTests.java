package com.base.tests;

import com.base.TestBase;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import databasefiles.ConnectToSqlDB;
import databasefiles.Employee;
import databasefiles.JsonClassObject;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import static databasefiles.ConnectToSqlDB.connection;

public class HWTests extends TestBase {
    public static ResultSet resultSet;

    @Test(enabled = true)
    public void validateUserCanRetrieveEmployeeAndMakeItToJson() throws SQLException, JsonProcessingException {

        ConnectToSqlDB.establishDBConnection();
        Statement statement = connection.createStatement();
        resultSet = statement.executeQuery("SELECT * from worldofautomation.employee;");

        ArrayList<Employee> listOfEmployees = new ArrayList<Employee>();
        while (resultSet.next()) {
          String name = resultSet.getString("name");
           String role = resultSet.getString("role");
            String city = resultSet.getString("city");
           int phone_number = resultSet.getInt("phone_number");
           int permanent = resultSet.getInt("permanent");
            Employee employee = new Employee(name, role, city, phone_number, permanent);
            listOfEmployees.add(employee);

        }
        //data from mysql employee
        System.out.println(listOfEmployees);

        //employee data to json
        String json = new ObjectMapper().writeValueAsString(listOfEmployees);
        System.out.println(json);

        //getting data from json file
        JSONArray jsonArray = getJsonArrayFromString(json);
        System.out.println(jsonArray);

        JSONObject jsonObject = (JSONObject) jsonArray.get(0);
        System.out.println(jsonObject);

    }

    @Test(enabled = false)
    public void validateUserCanCallJsonClass() throws JsonProcessingException {
        JSONArray jsonArray = getJsonArray("src/test/resources/test3.json");
        System.out.println(jsonArray);
        JSONObject jsonObject = (JSONObject) jsonArray.get(0);
        System.out.println(jsonObject);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonClassObject jsonClassObject = objectMapper.readValue(jsonObject.toJSONString(), JsonClassObject.class);
        System.out.println(jsonClassObject);
        System.out.println(jsonClassObject.getData());
        System.out.println(jsonClassObject.getFeature());
    }


    @Test(enabled = false)
    public void validateUserCanSolveAnObjectInsideAnotherJsonObject() {

        JSONArray jsonarray=getJsonArray("src/test/resources/test3.json");
        System.out.println(jsonarray);
        JSONObject jsonObject=(JSONObject) jsonarray.get(0);
        System.out.println(jsonObject);

        JSONObject jsonArrayData=(JSONObject) jsonObject.get("data");
        System.out.println(jsonArrayData);
    }


}
