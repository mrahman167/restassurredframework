package com.base.tests;

import com.base.ExtentTestManager;
import com.base.TestBase;
import com.fasterxml.jackson.core.JsonProcessingException;
import databasefiles.HwReturningEachColumnInAMap;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import java.util.logging.Logger;

public class DataBaseTestsAddress extends TestBase {
    private static final Logger logger = Logger.getLogger(String.valueOf(HwReturningEachColumnInAMap.class));


    @Test(enabled = false)
    public void validateUserCanRetrieveAllAddress() {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/address/all/";
        Response response = RestAssured.given().when().get(endpoint).then().log().all().extract().response();
        ExtentTestManager.log(response.asString());

    }


    @Test(enabled = false)
    public void validateUserCanGetSpecificAddress() {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/address/23";
        Response response = RestAssured.given().when().get(endpoint).then().log().all().extract().response();
        ExtentTestManager.log(response.asString());
    }

    @Test(enabled = false)
    public void validateUserCanDeleteAnAddress() {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/address/delete/23";
        Response response = RestAssured.given().when().delete(endpoint).then().log().all().extract().response();
        ExtentTestManager.log(response.asString());

    }

    @Test(enabled = false)
    public void validateUserCanCreateAnEmployee() throws JsonProcessingException {
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/address/add";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("employee_id", 3);
        jsonObject.put("street", "3343 102nd st");
        jsonObject.put("city", "queens");
        jsonObject.put("state", "NY");
        jsonObject.put("apt", "g23");
        jsonObject.put("zipcode", 11374);

        JSONArray jsonArray = new JSONArray();
        jsonArray.add(jsonObject);

        System.out.println(jsonArray.toJSONString());

        Response response = RestAssured
                .given().header("Content-Type", "application/json").when().body(jsonArray.toJSONString())
                .post(endpoint).then().log().all().extract().response();

        ExtentTestManager.log(response.asString());
    }

    @Test(enabled = true)
    public void validateUserCanUpdateAnAddress() {
        RestAssured.baseURI = "http://localhost:8090";
        String endpoint = "/address/update/3";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("employee_id", 4);
        jsonObject.put("street", "worldst");
        jsonObject.put("city", "manhattan");
        jsonObject.put("state", "NY");
        jsonObject.put("apt", "g23");
        jsonObject.put("zipcode", 11374);

        JSONArray jsonArray = new JSONArray();
        jsonArray.add(jsonObject);

        System.out.println(jsonArray.toJSONString());
        Response response = RestAssured.given().header("Content-Type", "application/json")
                .when().body(jsonObject.toJSONString()).put(endpoint).then()
                .log().all().extract().response();
        ExtentTestManager.log(response.asString());
    }

}
