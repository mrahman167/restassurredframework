package databasefiles;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class FileReaderUtils {

    public static String getPropertyOfFile(String filePath, String key) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(filePath));
        return properties.getProperty(key);
    }
}
