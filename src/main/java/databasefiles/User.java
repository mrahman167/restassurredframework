package databasefiles;

public class User {
private String name;
private String teamName;
private String salary;

    public User(String name, String teamName, String salary) {
        this.name = name;
        this.teamName = teamName;
        this.salary = salary;
    }


    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", teamName='" + teamName + '\'' +
                ", salary='" + salary + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }
}
