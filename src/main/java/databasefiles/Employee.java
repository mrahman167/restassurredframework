package databasefiles;

import lombok.Data;


public class Employee {
   private String name, role, city;
     private int phone_number,permanent;

    public Employee(String name, String role, String city, int phone_number, int permanent) {
        this.name = name;
        this.role = role;
        this.city = city;
        this.phone_number = phone_number;
        this.permanent = permanent;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", role='" + role + '\'' +
                ", city='" + city + '\'' +
                ", phone_number=" + phone_number +
                ", permanent=" + permanent +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(int phone_number) {
        this.phone_number = phone_number;
    }

    public int getPermanent() {
        return permanent;
    }

    public void setPermanent(int permanent) {
        this.permanent = permanent;
    }


}
