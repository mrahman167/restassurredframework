package databasefiles;

import com.base.ExtentTestManager;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class DataProvidersData {

    public static void dataa(Object data){
        RestAssured.baseURI = "http://localhost:8090/";
        String endpoint = "/employees/add";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "jawa");
        jsonObject.put("permanent", 1);
        jsonObject.put("phone_number", data);
        jsonObject.put("role", "QA");
        jsonObject.put("city", "Ozone Park");
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(jsonObject);
        System.out.println(jsonArray.toJSONString());
        Response response = RestAssured
                .given()
                .header("Content-Type", "application/json")
                .when()
                .body(jsonArray.toJSONString())
                .post(endpoint)
                .then()
                .log()
                .all()
                .extract().response();

        ExtentTestManager.log(response.asString());

    }

}
