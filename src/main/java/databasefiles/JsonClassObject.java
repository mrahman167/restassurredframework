package databasefiles;

import java.util.HashMap;

public class JsonClassObject {
    private String feature, timestamp;
    private int id;
    private HashMap<String, String> data;

    @Override
    public String toString() {
        return "JsonClassObject{" +
                "feature='" + feature + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", id=" + id +
                ", data=" + data +
                '}';
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public HashMap<String, String> getData() {
        return data;
    }

    public void setData(HashMap<String, String> data) {
        this.data = data;
    }
}